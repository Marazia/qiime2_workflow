import os
import zipfile
import click

def get_ext_path(path, ext):
    files = []
    # r=root, d=directories, f = files
    for r, d, f in os.walk(path):
        for file in f:
            if ext in file:
                files.append(os.path.join(r, file))
    return files

div = '''
     <div class="container-fluid bg-1 text-center">
        <object type="text/html" style="width:100%;height:500px;" data="./{0}" >
        </object>
    </div>
'''

html = '''
<!DOCTYPE html>
<html>

  <head>
  <!-- Latest compiled and minified CSS -->
      <title>Seriema</title>
      <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
      <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
  </head>

  <body>
    {0}
  </body>
</html>
'''

@click.command()
@click.option('--path', default='.',
                        help='Path to .qzv files')
@click.option('--mode', default='creation',
                        help='''How to order the files:
                                creation - creation time,
                                script - extract mention of file on script''')
@click.option('--script', default='',
                        help='Scritp that generated the qzv files')
@click.option('--out', default='qzv_bunch.html',
                        help='html file name')
def bunch_qzv(path, mode, script, out):
    qzv = get_ext_path(path, '.qzv')
    if mode=='creation':
        t_creation = [os.path.getmtime(x) for x in qzv]
        qzv = [x for _,x in sorted(zip(t_creation,qzv))]
    elif mode=='script':
        script = open(script).readlines()
        script = [x for x in script if '.qzv' in x]
        qtemp = []
        for s in script:
           for v in qzv:
               dn = v.split('/')[-1]
               if dn in s:
                   qtemp.append(v)

        #set(qzv)-set(qtemp)
        qzv = qtemp

    os.mkdir('qzv_bunch')

    html_list = []
    for v in qzv:
        dn = v.split('/')[-1].replace('.qzv', '')
        with zipfile.ZipFile(v, 'r') as zip_ref:
            zip_ref.extractall(os.path.join('qzv_bunch', dn))
            html_list.extend(get_ext_path(os.path.join('qzv_bunch', dn), 'index.html'))

    #html_list = get_ext_path('qzv_bunch/', 'index.html')
    div_list = [div.format(x) for x in html_list]
    with open(out, 'w') as f:
        f.write(html.format(''.join(div_list)))

if __name__ == '__main__':
    bunch_qzv()
