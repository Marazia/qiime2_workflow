## QIIME2 WORKFLOW

This repository hosts scripts to qiime2 workflow and Slurm Workload Manager.

To run the script install the latest version o [qiime2](https://docs.qiime2.org/2019.7/install/native/#install-qiime-2-within-a-conda-environment)

```
wget https://data.qiime2.org/distro/core/qiime2-2019.7-py36-linux-conda.yml
conda env create -n qiime2-2019.7 --file qiime2-2019.7-py36-linux-conda.yml
# OPTIONAL CLEANUP
rm qiime2-2019.7-py36-linux-conda.yml
```

Then, on the same environment install [MultiQC](https://multiqc.info)

```
conda activate qiime2-2019.7
pip install multiqc
```

`multiqc` does not support qiime2 output, but we are going to use the same concepts.

The goal is to modify a [snakemake workflow](https://github.com/shu251/tagseq-qiime2-snakemake/tree/master) to fit the project's need.
